# Pointcloud Transform Application
This package provides a node to transform depth images into 3D point clouds in ROS2. This package also includes RViz2 visualization configurations to compare the depth image and resulting point cloud. This packages is catered toward usage with the OADK-D S2 camera, which assumes the depth image data is in millimeters and image data format is `16UC1`.

## Installation

### From Docker Image
1. Pull and the docker image hosted on the GitLab repo:
```
docker pull registry.gitlab.com/raymond.g.andrade/pointcloud_ta/pointcloud_ta:latest
```
Note: Feel free to retag the image name to something more convenient for you.

2. Run the docker image:
```
docker run -it --rm --net=host registry.gitlab.com/raymond.g.andrade/pointcloud_ta/pointcloud_ta:latest
```

3. Customization:

By default the previous command will run the depth_to_pointcloud node with the default parameters. If you wish to run the node within the docker environment with custom parameters, you can override the base command like so:
```
docker run -it --rm --net=host registry.gitlab.com/raymond.g.andrade/pointcloud_ta/pointcloud_ta:latest bash

# Within the docker environment
source /ros_entrypoint.sh
source /ros_ws/install/setup.bash
ros2 run pointcloud_ta depth_to_pointcloud --ros-args -p <parameter_name>:=<value>
```
See the [Customizeable Parameters](#customizeable-parameters) section for the available parameters. 

### From Source
1. Clone the repository in your ROS2 workspace:
```
cd <path_to_your_ws>/src/
git clone https://gitlab.com/raymond.g.andrade/pointcloud_ta.git
```

2. Build the package:
```
cd <path_to_your_ws>
colcon build --symlink-install --packages-select pointcloud_ta
```

3. Source the workspace:
```
source <path_to_your_ws>/install/setup.bash
```

## Usage
There are two versions of the node, one done in Python and the other in C++. The C++ version is the default and recommended version to use. The Python version is provided for reference, performance comparison, and easy editing. You may easily configure both nodes by customizing the node parameters. 

### Running the Node
C++ version:
```
ros2 run pointcloud_ta depth_to_pointcloud
```

Python version:
```
ros2 run pointcloud_ta depth_to_pointcloud.py
```

Scroll down to the [Customizeable Parameters](#customizeable-parameters) section to see the available parameters that can be used to furthur configure the node.

### Launching The Node With RViz2 Configurations 
The launch file will launch the depth_to_pointcloud node as well as open a RViz instance with the configurations to visualize the depth image and pointcloud. A world->oakd_transform is configured for convinient viewing in RViz. The launch file also provides a way to configure to use either the C++ (default) or Python version of the node.

```
ros2 launch pointcloud_ta depth_to_pointcloud_rviz.launch.py language:=<cpp|python>
```


## Customizeable Parameters
```
ros2 run pointcloud_ta depth_to_pointcloud --ros-args -p <parameter_name1>:=<value1> -p <parameter_name2>:=<value2>
```

| Parameter(s) | Description |
| --- | --- |
| `fx`, `fy` | Camera intrinsics: Focal lengths (X, Y) |
| `cx`, `cy` | Camera intrinsics: Optical center (X, Y) |
| `depth_image_topic` | ROS topic for the incoming depth image |
| `pointcloud_topic` | ROS topic to publish the generated pointcloud |
| `tf_world_enable` | Boolean flag to control whether a static transform to the world frame is published |
| `tf_world_frame_id`, `tf_child_frame_id` | Frame IDs for the published static transform |
| `tf_t_x`, `tf_t_y`, `tf_t_z` | Pointcloud transform translation (X, Y, Z) |
| `tf_q_x`, `tf_q_y`, `tf_q_z`, `tf_q_w` | Pointcloud transform rotation quaternion (X, Y, Z, W) |
| `min_depth_val`, `max_depth_val` | Acceptable depth range (in meters) to convert into pointcloud |


## Extra Resources
- [Video Showcasing Usage](https://rebrand.ly/vta_showcase_vid)
- [ROS2Bag File Of Resulting Pointcloud](https://rebrand.ly/vta_bagfile)