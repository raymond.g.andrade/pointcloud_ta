ARG ROS_DISTRO=humble

FROM osrf/ros:${ROS_DISTRO}-desktop

WORKDIR /ros_ws

COPY ./pointcloud_ta ./src/pointcloud_ta
COPY ./startup.sh /

RUN chmod +x /startup.sh
RUN chmod +x /ros_ws/src/pointcloud_ta/scripts/*.py

RUN apt-get update && apt-get upgrade -y \
    && apt-get install -y python3-colcon-common-extensions \
    && . /opt/ros/${ROS_DISTRO}/setup.sh \
    && rosdep update \
    && rosdep install --from-paths src --ignore-src -r -y \
    && colcon build --symlink-install \
    && rm -rf /var/lib/apt/lists/*  

ENTRYPOINT ["/ros_entrypoint.sh"]
CMD ["/startup.sh"]