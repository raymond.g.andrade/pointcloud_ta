#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from std_msgs.msg import Header
from sensor_msgs.msg import Image, PointCloud2
from sensor_msgs_py import point_cloud2
import numpy as np
import cv2

from tf2_ros.static_transform_broadcaster import StaticTransformBroadcaster
from geometry_msgs.msg import TransformStamped

class DepthToPointCloudNode(Node):
    def __init__(self):
        super().__init__('depth_to_pointcloud_py')

        # Camera intrinsic parameters
        self.declare_parameter("fx", 400.00485229)
        self.declare_parameter("fy", 400.00485229)
        self.declare_parameter("cx", 323.98687744)
        self.declare_parameter("cy", 200.87487793)

        # Topic Parameters
        self.declare_parameter("depth_image_topic", "/stereo/depth")
        self.declare_parameter("pointcloud_topic", "pointcloud")

        # Transform parameters
        self.declare_parameter("tf_world_enable", True)
        self.declare_parameter("tf_world_frame_id", "world")
        self.declare_parameter("tf_child_frame_id", "oakd_frame")
        self.declare_parameter("tf_t_x", 0.0)
        self.declare_parameter("tf_t_y", 0.0)
        self.declare_parameter("tf_t_z", 1.25)
        self.declare_parameter("tf_q_x", -1.0)
        self.declare_parameter("tf_q_y", 0.0)
        self.declare_parameter("tf_q_z", 0.0)
        self.declare_parameter("tf_q_w", 1.0)

        self.declare_parameter("min_depth_val", 0.0)
        self.declare_parameter("max_depth_val", 2.5)

        fx = self.get_parameter("fx").value
        fy = self.get_parameter("fy").value
        cx = self.get_parameter("cx").value
        cy = self.get_parameter("cy").value

        self.tf_child_frame_id = self.get_parameter("tf_child_frame_id").value

        self.min_depth_val = self.get_parameter("min_depth_val").value
        self.max_depth_val = self.get_parameter("max_depth_val").value

        
        self.publisher_ = self.create_publisher(PointCloud2, self.get_parameter("pointcloud_topic").value, 10)
        self.subscription = self.create_subscription(
            Image,
            self.get_parameter("depth_image_topic").value,
            self.listener_callback,
            10
        )
        

        # Camera intrinsic matrix
        self.camera_intrinsics = np.array([[fx, 0., cx],
                                           [0., fy, cy],
                                           [0., 0., 1.]])
        
        self.broadcaster = StaticTransformBroadcaster(self)

        if self.get_parameter("tf_world_enable").value:
            t = TransformStamped()
            t.header.stamp = self.get_clock().now().to_msg()
            t.header.frame_id = self.get_parameter("tf_world_frame_id").value
            t.child_frame_id = self.tf_child_frame_id
            t.transform.translation.x = self.get_parameter("tf_t_x").value
            t.transform.translation.y = self.get_parameter("tf_t_y").value
            t.transform.translation.z = self.get_parameter("tf_t_z").value
            t.transform.rotation.x = self.get_parameter("tf_q_x").value
            t.transform.rotation.y = self.get_parameter("tf_q_y").value
            t.transform.rotation.z = self.get_parameter("tf_q_z").value
            t.transform.rotation.w = self.get_parameter("tf_q_w").value

        self.broadcaster.sendTransform(t)

    def listener_callback(self, msg):
        # process raw values into a 2D depth image
        depth_image = self.decode_depth_image(msg)
        # convert depth image to point cloud and publish
        pc = self.depth_image_to_point_cloud(depth_image)
        self.publisher_.publish(pc)

    def decode_depth_image(self, msg):
        # change shape from an array to 2D image so cv2 can undistort it
        depth_img = np.array(msg.data, dtype=np.uint8).reshape(msg.height, msg.width, -1)
        depth_img_16UC1 = np.uint16(depth_img[:,:,0]) + np.uint16(depth_img[:,:,1]) * 256
        return cv2.undistort(depth_img_16UC1, self.camera_intrinsics, None)

    def depth_image_to_point_cloud(self, depth):
        rows, cols = depth.shape
        c, r = np.meshgrid(np.arange(cols), np.arange(rows), sparse=True)

        # Only keep desired depth values, convert from meters to millimeters
        valid = (depth > self.min_depth_val * 1000) & (depth < self.max_depth_val * 1000) 
        Z = np.where(valid, depth / 1000.0, np.nan)
        X = np.where(valid, Z * (c - self.camera_intrinsics[0, 2]) / self.camera_intrinsics[0, 0], 0)
        Y = np.where(valid, Z * (r - self.camera_intrinsics[1, 2]) / self.camera_intrinsics[1, 1], 0)

        # Set up header/frame for transfrom capabilities
        header = Header()
        header.stamp = self.get_clock().now().to_msg()
        header.frame_id = self.tf_child_frame_id

        pc2_msg = point_cloud2.create_cloud_xyz32(header, np.dstack((X, Y, Z)))
        
        return pc2_msg

def main(args=None):
    rclpy.init(args=args)

    depth_to_pointcloud_node = DepthToPointCloudNode()

    try: 
        rclpy.spin(depth_to_pointcloud_node)
    except KeyboardInterrupt:
        print("KeyboardInterrupt detected, shutting down node")
    finally:
        depth_to_pointcloud_node.destroy_node()


if __name__ == '__main__':
    main()
