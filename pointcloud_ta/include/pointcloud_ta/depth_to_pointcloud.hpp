#ifndef DEPTH_TO_POINTCLOUD_H_
#define DEPTH_TO_POINTCLOUD_H_

#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/point_cloud2.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "tf2_ros/static_transform_broadcaster.h"
#include <cv_bridge/cv_bridge.h>

class DepthToPointCloudNode : public rclcpp::Node
{
public:
    DepthToPointCloudNode();

private:
    void imageCallback(const sensor_msgs::msg::Image::SharedPtr msg);
    sensor_msgs::msg::PointCloud2::SharedPtr depthToPointCloud(const cv::Mat &depth_img);

    rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr image_sub_;
    rclcpp::Publisher<sensor_msgs::msg::PointCloud2>::SharedPtr pointcloud_pub_;
    std::shared_ptr<tf2_ros::StaticTransformBroadcaster> broadcaster_;

    // cv::Mat camera_intrinsics_;
    double fx_, fy_, cx_, cy_, min_depth_val_, max_depth_val_;

    std::string tf_child_frame_id_;
};

#endif // DEPTH_TO_POINTCLOUD_H_