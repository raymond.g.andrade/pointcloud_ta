import os
import sys
from launch import LaunchDescription
from launch_ros.actions import Node
from ament_index_python.packages import get_package_share_directory
import launch.actions
from launch.conditions import IfCondition
from launch.substitutions import LaunchConfiguration, PythonExpression

def generate_launch_description():
    # Configure executable to be either Python or C++ versions of the node
    language_arg = launch.actions.DeclareLaunchArgument(
        'language',
        default_value='cpp',
        choices=['cpp', 'python', 'CPP', 'Python'],
        description='Version of the node to launch: python or cpp'
    )

    executable_name = PythonExpression([
        "\"depth_to_pointcloud.py\" if '", 
        PythonExpression(["'", LaunchConfiguration('language'), "'.lower()"]),
        "' == \"python\" else \"depth_to_pointcloud\""
    ])

    # Get the location of the RViz config file
    pointcloud_ta_share_dir = get_package_share_directory('pointcloud_ta')
    rviz_config_file_path = os.path.join(pointcloud_ta_share_dir, 'config', 'depth_pointcloud_rviz_config.rviz')

    return LaunchDescription([
        language_arg,
        Node(
            package='pointcloud_ta',
            executable=executable_name,
            output='screen'
        ),
        Node(
            package='rviz2',
            executable='rviz2',
            output='screen',
            arguments=['-d', rviz_config_file_path]
        )
    ])