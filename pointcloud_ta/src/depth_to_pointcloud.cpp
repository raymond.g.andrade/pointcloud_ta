#include "pointcloud_ta/depth_to_pointcloud.hpp"
#include <opencv2/imgproc/imgproc.hpp>

#include <sensor_msgs/msg/point_cloud2.hpp>
#include <sensor_msgs/point_cloud2_iterator.hpp>

DepthToPointCloudNode::DepthToPointCloudNode()
: Node("depth_to_pointcloud")
{
    // Configurable Parameters
    this->declare_parameter<double>("fx", 400.00485229);
    this->declare_parameter<double>("fy", 400.00485229);
    this->declare_parameter<double>("cx", 323.98687744);
    this->declare_parameter<double>("cy", 200.87487793);

    this->declare_parameter<std::string>("depth_image_topic", "/stereo/depth");
    this->declare_parameter<std::string>("pointcloud_topic", "pointcloud");

    this->declare_parameter<bool>("tf_world_enable", true);
    this->declare_parameter<std::string>("tf_world_frame_id", "world");
    this->declare_parameter<std::string>("tf_child_frame_id", "oakd_frame");
    this->declare_parameter<double>("tf_t_x", 0.0);
    this->declare_parameter<double>("tf_t_y", 0.0);
    this->declare_parameter<double>("tf_t_z", 1.25);
    this->declare_parameter<double>("tf_q_x", -1.0);
    this->declare_parameter<double>("tf_q_y", 0.0);
    this->declare_parameter<double>("tf_q_z", 0.0);
    this->declare_parameter<double>("tf_q_w", 1.0);

    this->declare_parameter<double>("min_depth_val", 0.0);
    this->declare_parameter<double>("max_depth_val", 2.5);

    this->get_parameter("fx", fx_);
    this->get_parameter("fy", fy_);
    this->get_parameter("cx", cx_);
    this->get_parameter("cy", cy_);

    this->get_parameter("min_depth_val", min_depth_val_);
    this->get_parameter("max_depth_val", max_depth_val_);

    std::string depth_image_topic, pointcloud_topic;
    this->get_parameter("depth_image_topic", depth_image_topic);
    this->get_parameter("pointcloud_topic", pointcloud_topic);

    bool tf_world_enable;
    this->get_parameter("tf_world_enable", tf_world_enable);

    std::string tf_world_frame_id;
    this->get_parameter("tf_world_frame_id", tf_world_frame_id);
    this->get_parameter("tf_child_frame_id", tf_child_frame_id_);

    double tf_t_x, tf_t_y, tf_t_z, tf_q_x, tf_q_y, tf_q_z, tf_q_w;
    this->get_parameter("tf_t_x", tf_t_x);
    this->get_parameter("tf_t_y", tf_t_y);
    this->get_parameter("tf_t_z", tf_t_z);
    this->get_parameter("tf_q_x", tf_q_x);
    this->get_parameter("tf_q_y", tf_q_y);
    this->get_parameter("tf_q_z", tf_q_z);
    this->get_parameter("tf_q_w", tf_q_w);


    image_sub_ = this->create_subscription<sensor_msgs::msg::Image>(
        depth_image_topic, 10, std::bind(&DepthToPointCloudNode::imageCallback, this, std::placeholders::_1));

    pointcloud_pub_ = this->create_publisher<sensor_msgs::msg::PointCloud2>(pointcloud_topic, 10);

    if (tf_world_enable)
    {
      broadcaster_ = std::make_shared<tf2_ros::StaticTransformBroadcaster>(this);

      geometry_msgs::msg::TransformStamped transformStamped;

      transformStamped.header.stamp = this->now();
      transformStamped.header.frame_id = tf_world_frame_id;
      transformStamped.child_frame_id = tf_child_frame_id_;
      transformStamped.transform.translation.x = tf_t_x;
      transformStamped.transform.translation.y = tf_t_y;
      transformStamped.transform.translation.z = tf_t_z;
      transformStamped.transform.rotation.x = tf_q_x;
      transformStamped.transform.rotation.y = tf_q_y;
      transformStamped.transform.rotation.z = tf_q_z;
      transformStamped.transform.rotation.w = tf_q_w;

      broadcaster_->sendTransform(transformStamped);
    }
}

void DepthToPointCloudNode::imageCallback(const sensor_msgs::msg::Image::SharedPtr msg)
{
    cv_bridge::CvImagePtr cv_ptr;

    try {
        cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::TYPE_16UC1);
    } catch (cv_bridge::Exception& e) {
        RCLCPP_ERROR(this->get_logger(), "cv_bridge exception: %s", e.what());
        return;
    }

    auto depth_img = cv_ptr->image;
    auto pointcloud_msg = depthToPointCloud(depth_img);
    pointcloud_pub_->publish(*pointcloud_msg);
}

sensor_msgs::msg::PointCloud2::SharedPtr DepthToPointCloudNode::depthToPointCloud(const cv::Mat &depth_img)
{
   // Create a new PointCloud2 message
  auto cloud_msg = std::make_shared<sensor_msgs::msg::PointCloud2>();
  
  // Initialize the PointCloud2 modifier
  sensor_msgs::PointCloud2Modifier modifier(*cloud_msg);
  
  // Set the fields for x, y, and z
  modifier.setPointCloud2FieldsByString(1, "xyz");
  
  // Resize the PointCloud2 message to hold the number of points
  modifier.resize(depth_img.rows * depth_img.cols);
  
  // Define the iterators
  sensor_msgs::PointCloud2Iterator<float> iter_x(*cloud_msg, "x");
  sensor_msgs::PointCloud2Iterator<float> iter_y(*cloud_msg, "y");
  sensor_msgs::PointCloud2Iterator<float> iter_z(*cloud_msg, "z");

  for (int v = 0; v < depth_img.rows; v++) {
    for (int u = 0; u < depth_img.cols; u++) {
      // Retrieve the depth value
      uint16_t depth = depth_img.at<uint16_t>(v, u);

      if (depth > min_depth_val_ * 1000 && depth < max_depth_val_ * 1000) {
        // Calculate the 3D space coordinates
        double Z = depth / 1000.0;
        double X = (u - cx_) * Z / fx_;
        double Y = (v - cy_) * Z / fy_;
        
        // Assign the 3D coordinates to the PointCloud2 message using the iterators
        *iter_x = static_cast<float>(X);
        *iter_y = static_cast<float>(Y);
        *iter_z = static_cast<float>(Z);
      } else {
        *iter_x = std::numeric_limits<float>::quiet_NaN();
        *iter_y = std::numeric_limits<float>::quiet_NaN();
        *iter_z = std::numeric_limits<float>::quiet_NaN();
      }
      
      // Increment the iterators
      ++iter_x;
      ++iter_y;
      ++iter_z;
    }
  }
  
  // Fill in the header information for the transform functionality
  cloud_msg->header.stamp = this->get_clock()->now();
  cloud_msg->header.frame_id = tf_child_frame_id_;
  
  return cloud_msg;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<DepthToPointCloudNode>();
    rclcpp::spin(node);

    rclcpp::shutdown();
    return 0;
}